class RenameMicropostColumnToNotifications < ActiveRecord::Migration[5.2]
  def change
    rename_column :notifications, :micropost_id, :post_id
  end
end
