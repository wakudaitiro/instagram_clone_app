Rails.application.routes.draw do

  get 'bookmarks/create'
  get 'bookmarks/destroy'
  devise_for :users, :controllers => {
    :registrations => 'users/registrations'

   }
   resource :user, only: %i[edit] do
    collection do
      patch 'update_password'
    end
  end

  root 'posts#index'

  get '/users/:id', to: 'users#show', as: 'user'

  get 'posts/search', to: 'posts#search', as: 'search'

  resources :users do
    member do
      get :following,:followers
    end
  end

  resources :posts, only: %i(index new create show destroy) do
    resources :photos, only: %i(create)
    resources :likes, only: %i(create destroy)
    resources :comments, only: %i(create destroy)
    resources :bookmarks, only: %i[create destroy]
  end

   resources :relationships,       only: [:create, :destroy]
   resources :notifications,       only: [:index, :destroy]
end
